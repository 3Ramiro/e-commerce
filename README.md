# CakePHP Application

Sistema e-commerce desarrollado con [CakePHP](https://book.cakephp.org/3.0/en/index.html).

Este programa cumple con la elaboración de material didáctico para las asignaturas en el área de algoritmos, programación, aplicaciones web y afines.

## Instalación

1. Descargar [Git](https://git-scm.com/downloads) y clonar este proyecto Remoto en su máquina local.
2. Instalar requerimientos para [CakePhp](https://book.cakephp.org/3.0/en/installation.html).
3. Abre la una Terminal y dirige hasta la caperta `e-commerce` que fue clonada.
4. Ejecuta el comando `bin/cake server`.
5. Abre el navegador en localhost: `puerto`.
